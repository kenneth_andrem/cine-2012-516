<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Complejo</title>
	  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
	  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
	  <link href="css/owl.carousel.css" type="text/css" rel="stylesheet" media="screen,projection"/>
	  <link href="css/owl.theme.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="css/owl.transitions.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="css/Xtra2.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    
    <style>
      #map-canvas {
        width: 800px;
        height: 400px;
      }
    </style>
    
    
</head>
<body>

<body class="blue-grey darken-4">
<div class="navbar-fixed">
  <nav class="animated fadeInUpBig black">
  	<div class="container">
    <div class="nav-wrapper">
      <div class="col s12">
        <a href="./servletBienvenida.do" class="brand-logo white-text"><img alt="Cine Kinal" src="resources/icon-kenneth-cine.png"></a>
        <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="mdi-navigation-menu"></i></a>
        <ul class="right hide-on-med-and-down">
          <li><a href="sass.html" class="yellow-text">Sass</a></li>
          <li><a href="components.html" class="yellow-text">Components</a></li>
          <li><a href="javascript.html" class="yellow-text">Javascript</a></li>
          <li><a href="mobile.html" class="yellow-text">Mobile</a></li>
        </ul>
        <ul class="side-nav yellow" id="mobile-demo">
          <li><a href="sass.html">Sass</a></li>
          <li><a href="components.html">Components</a></li>
          <li><a href="javascript.html">Javascript</a></li>
          <li><a href="mobile.html">Mobile</a></li>
        </ul>
      </div>
    </div>
    </div>
  </nav>
</div>

  <div id="index-banner" class="parallax-container animated fadeInUpBig">
    <div class="section no-pad-bot">
      <div class="container">
        <br><br>
        <h1 class="header center teal-text text-lighten-2 ">Cine</h1>
        <div class="row center">
          <h5 class="header col s12 light">Una nueva y mejor manera de visitar un cinedesde tu casa</h5>
        </div>
        <div class="row center">
          <a href="./servletBienvenida.do" id="download-button" class="btn-large waves-effect waves-light teal lighten-1">Inicio</a>
        </div>
        <br><br>
      </div>
    </div>
    <div class="parallax"><img src="resources/tickets.jpg" alt="Unsplashed background img 2"></div>
  </div>
	
	<c:forEach items="${complejos}" var="complejo">
	<div class="container">
	  <div class="row">
        <div class="col s12">
          <div class="card blue-grey darken-1">
            <div class="card-content white-text">
              <span class="card-title"><h2>${complejo.getNombreComplejo() }</h2></span>
              <h3> Direcci�n del complejo: </h3>
              <p>${complejo.getDireccionComplejo()}</p>
              <div id="map-canvas"></div>
            </div>
            <div class="card-action">
              <a href="#">This is a link</a>
              <a href='#'>This is a link</a>
            </div>
          </div>
        </div>
      </div>
  	</div>
  	</c:forEach>
  	
  	
  
     <div class="fixed-action-btn animated fadeInUpBig" style="bottom: 150px; right: 100px;">
    <a class="btn-floating btn-large yellow darken-2">
      <i class="large mdi-image-movie-creation"></i>
    </a>
    <ul>
      <li><a href="./servletListaPeliculas.do" class="btn-floating red"><i class="large mdi-maps-local-movies"></i></a></li>
      <li><a class="btn-floating yellow darken-1"><i class="large mdi-editor-format-quote"></i></a></li>
      <li><a class="btn-floating green"><i class="large mdi-action-add-shopping-cart"></i></a></li>
      <li><a class="btn-floating blue"><i class="large mdi-social-mood"></i></a></li>
    </ul>
  </div>
  
  
  
   <footer class="page-footer yellow">
    <div class="footer-copyright">
      <div class="container">
      <p>Made by <a href="http://kennethandre.jimdo.com">Kenneth Andr� Mart�nez Molina</a></p>
      </div>
    </div>
  </footer>
  
  

<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>
  <script src="js/owl.carousel.min.js" ></script>
  <script>
  $(document).ready(function() {
 
  var owl = $("#owl-demo");
 
  owl.owlCarousel({
      items : 5, //10 items above 1000px browser width
      itemsDesktop : [1000,5], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,3], // betweem 900px and 601px
      itemsTablet: [600,2], //2 items between 600 and 0
      itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
  });
 
  // Custom Navigation Events
  $(".next").click(function(){
    owl.trigger('owl.next');
  })
  $(".prev").click(function(){
    owl.trigger('owl.prev');
  })
  $(".play").click(function(){
    owl.trigger('owl.play', 1100); //owl.play event accept autoPlay speed as second parameter
  })
  $(".stop").click(function(){
    owl.trigger('owl.stop');
  })
 
});  
  
  </script>
  
  <script src="https://maps.googleapis.com/maps/api/js"></script>
						    <script>
						    var map;
						    var TILE_SIZE = 256;
						    var chicago = new google.maps.LatLng(14.5982548,-90.5259489);
						
						    function bound(value, opt_min, opt_max) {
						      if (opt_min != null) value = Math.max(value, opt_min);
						      if (opt_max != null) value = Math.min(value, opt_max);
						      return value;
						    }
						
						    function degreesToRadians(deg) {
						      return deg * (Math.PI / 180);
						    }
						
						    function radiansToDegrees(rad) {
						      return rad / (Math.PI / 180);
						    }
						
						    /** @constructor */
						    function MercatorProjection() {
						      this.pixelOrigin_ = new google.maps.Point(TILE_SIZE / 2,
						          TILE_SIZE / 2);
						      this.pixelsPerLonDegree_ = TILE_SIZE / 360;
						      this.pixelsPerLonRadian_ = TILE_SIZE / (2 * Math.PI);
						    }
						
						    MercatorProjection.prototype.fromLatLngToPoint = function(latLng,
						        opt_point) {
						      var me = this;
						      var point = opt_point || new google.maps.Point(0, 0);
						      var origin = me.pixelOrigin_;
						
						      point.x = origin.x + latLng.lng() * me.pixelsPerLonDegree_;
						
						      // Truncating to 0.9999 effectively limits latitude to 89.189. This is
						      // about a third of a tile past the edge of the world tile.
						      var siny = bound(Math.sin(degreesToRadians(latLng.lat())), -0.9999,
						          0.9999);
						      point.y = origin.y + 0.5 * Math.log((1 + siny) / (1 - siny)) *
						          -me.pixelsPerLonRadian_;
						      return point;
						    };
						
						    MercatorProjection.prototype.fromPointToLatLng = function(point) {
						      var me = this;
						      var origin = me.pixelOrigin_;
						      var lng = (point.x - origin.x) / me.pixelsPerLonDegree_;
						      var latRadians = (point.y - origin.y) / -me.pixelsPerLonRadian_;
						      var lat = radiansToDegrees(2 * Math.atan(Math.exp(latRadians)) -
						          Math.PI / 2);
						      return new google.maps.LatLng(lat, lng);
						    };
						
						    function createInfoWindowContent() {
						      var numTiles = 1 << map.getZoom();
						      var projection = new MercatorProjection();
						      var worldCoordinate = projection.fromLatLngToPoint(chicago);
						      var pixelCoordinate = new google.maps.Point(
						          worldCoordinate.x * numTiles,
						          worldCoordinate.y * numTiles);
						      var tileCoordinate = new google.maps.Point(
						          Math.floor(pixelCoordinate.x / TILE_SIZE),
						          Math.floor(pixelCoordinate.y / TILE_SIZE));
						
						      return [
						        'Chicago, IL',
						        'LatLng: ' + chicago.lat() + ' , ' + chicago.lng(),
						        'World Coordinate: ' + worldCoordinate.x + ' , ' +
						          worldCoordinate.y,
						        'Pixel Coordinate: ' + Math.floor(pixelCoordinate.x) + ' , ' +
						          Math.floor(pixelCoordinate.y),
						        'Tile Coordinate: ' + tileCoordinate.x + ' , ' +
						          tileCoordinate.y + ' at Zoom Level: ' + map.getZoom()
						      ].join('<br>');
						    }
						
						    function initialize() {
						      var mapOptions = {
						        zoom: 3,
						        center: chicago
						      };
						
						      map = new google.maps.Map(document.getElementById('map-canvas'),
						          mapOptions);
						
						      var coordInfoWindow = new google.maps.InfoWindow();
						      coordInfoWindow.setContent(createInfoWindowContent());
						      coordInfoWindow.setPosition(chicago);
						      coordInfoWindow.open(map);
						
						      google.maps.event.addListener(map, 'zoom_changed', function() {
						        coordInfoWindow.setContent(createInfoWindowContent());
						        coordInfoWindow.open(map);
						      });
						    }
						
						    google.maps.event.addDomListener(window, 'load', initialize);
						        </script>
  
<script type="text/javascript">
$(document).ready(function(){
    $('ulh.tabs').tabs();
  });
  
var CMD_CODE = 91;
var CTRL_CODE = 17;
var SHIFT_CODE = 16;
var DRAG_THRESHOLD = 5;
var items = $("lih");
var ghost = $(".ghost");

var state = {
  dragging: false	,
  mousedown: false	,
  multiselect: false,
  rangeselect: false,
  last_clicked: null
}


$(document).on('keydown', function(e){
  if (e.keyCode == CMD_CODE || e.keyCode == CTRL_CODE) {
    state.multiselect = true;
  }
  if (e.keyCode == SHIFT_CODE) {
        state.rangeselect = true;
  }
}).on('keyup', function(e){
  if (e.keyCode == CMD_CODE || e.keyCode == CTRL_CODE) {
    state.multiselect = false;
  }
  if (e.keyCode == SHIFT_CODE) {
    state.rangeselect = false;
  }
});


//bind tile click
items.on('click', function() {
    update_focus($(this));
});


function update_focus(el) {
  var count = $('lih.focus').length;
  //shift key
  if (!state.multiselect && state.rangeselect) {
    if (!state.last_clicked) {
      items.not(el).removeClass('focus');
      el.toggleClass('focus');
      state.last_clicked = el;
    } else {
      items.removeClass('focus');
      var bounds = el.add(state.last_clicked);
      var range = bounds.first().nextUntil(bounds.last());
      range.addClass('focus');
      bounds.addClass('focus');
    }
  }
  //cmd/ctrl key
  else if (state.multiselect && !state.rangeselect) {
    el.toggleClass('focus');
    state.last_clicked = el;
  }
  //no modifiers
  else {
    items.not(el).removeClass('focus');
    if (count > 1) el.addClass('focus');
    else el.toggleClass('focus');
    state.last_clicked = el;
  }
}


init_drag_selection($(document));

//initialize dragging transparent ghost element 
function init_drag_selection(el){
  var x_start = 0;
  var y_start = 0;
  var w = 0;
  var h = 0;

  el.on('mousedown', function(e){
    x_start = e.pageX;
    y_start = e.pageY;
    state.mousedown = true;
    ghost.css({'left':x_start, 'top':y_start });
  });

  el.on("mouseup", function(e){
    state.mousedown = false;
    ghost.css({ 'width':0, 'height':0 });  
    if(state.dragging){
      select_inside(ghost.css('left'), ghost.css('top'), w, h); 
      state.dragging = false;
    }
    //deselect by clicking outside a tile
    else{
      if(!$(e.target).hasClass('tile')){
      		  items.removeClass('focus');  
      }
    }
  });

  el.on("mousemove", function(e){
    if(state.mousedown){
      w = Math.abs(x_start - e.pageX);
      h = Math.abs(y_start - e.pageY);

      if((w>DRAG_THRESHOLD && h>DRAG_THRESHOLD) && !state.dragging){
        state.dragging = true;
      }
      
      ghost.css({ 'width':w, 'height':h });
      if (e.pageX <= x_start && e.pageY >= y_start) {
        ghost.css({'left': e.pageX});
      } else if (e.pageY <= y_start && e.pageX >= x_start) {
        ghost.css({'top': e.pageY });
      } else if (e.pageY < y_start && e.pageX < x_start) {
        ghost.css({ 'left': e.pageX, "top": e.pageY });
      }
    }
  });
}

//select the files that are inside the
//ghost element when releaseing the mouse
function select_inside(x, y, w, h){
  var box = {
    top:parseInt(y),
    left:parseInt(x),
    bottom:parseInt(y)+parseInt(h),
    right:parseInt(x)+parseInt(w)
  }

  if(!state.multiselect && !state.rangeselect){
    		items.removeClass('focus');  
  }
  items.each(function(){
    var inside = (function(el){
      var tile = el.getBoundingClientRect();
      //check intersection
      return (
        (tile.left <= box.right) &&
        (box.left <= tile.right) &&
        (tile.top <= box.bottom) &&
        (box.top <= tile.bottom)
      );
    })(this);

    if(inside){
      $(this).addClass('focus');
    }
  });
}


       
</script>
</body>
</html>