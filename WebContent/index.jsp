<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
  <title>Cine - Kenneth</title>

  <!-- CSS  -->
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/owl.carousel.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/owl.theme.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="css/owl.transitions.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    
    
    <style>
    #owl-demo .item{
  padding: 30px 0px;
  margin: 10px;
  color: #FFF;
  -webkit-border-radius: 3px;
  -moz-border-radius: 3px;
  border-radius: 3px;
  text-align: center;
}
.customNavigation{
  text-align: center;
}
//use styles below to disable ugly selection
.customNavigation a{
  -webkit-user-select: none;
  -khtml-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
}
    </style>
    
</head>
<body class="blue-grey darken-4">
<div class="navbar-fixed">
  <nav class="black">
  	<div class="container">
    <div class="nav-wrapper">
      <div class="col s12">
        <a href="#!" class="brand-logo white-text"><img alt="Cine Kinal" src="resources/icon-kenneth-cine.png"></a>
        <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="mdi-navigation-menu"></i></a>
        <ul class="right hide-on-med-and-down">
          <li><a href="" class="yellow-text">Conocenos</i></a></li>
          <li><a href="components.html" class="yellow-text">Components</a></li>
          <li><a href="javascript.html" class="yellow-text">Javascript</a></li>
          <li><a href="mobile.html" class="yellow-text">Mobile</a></li>
        </ul>
        <ul class="side-nav yellow" id="mobile-demo">
          <li><a href="sass.html">Sass</i></a></li>
          <li><a href="components.html">Components</a></li>
          <li><a href="javascript.html">Javascript</a></li>
          <li><a href="mobile.html">Mobile</a></li>
        </ul>
      </div>
    </div>
    </div>
  </nav>
</div>

  <div id="index-banner" class="parallax-container">
    <div class="section no-pad-bot">
      <div class="container">
        <br><br>
        <h1 class="header center yellow-text text-lighten-2">Cine</h1>
        <div class="row center">
          <h5 class="header col s12 light">Una nueva y mejor manera de visitar un cinedesde tu casa</h5>
        </div>
        <div class="row center">
          <a href="#" id="download-button" class="btn-large waves-effect waves-yellow blue-grey">Get Started</a>
        </div>
        <br><br>

      </div>
    </div>
    <div class="parallax"><img src="resources/tickets.jpg" alt="Unsplashed background img 2"></div>
  </div>


  <div class="container animated fadeInUpBig">
    <div class="section">
		
			<div id="owl-demo" class="owl-carousel owl-theme">
			<c:forEach items="${bienvenida}" var="bienvenidaCart">
		  		<div class="item"><img src="${bienvenidaCart.getImagen()}" alt="Touch"/></div>
		  		</c:forEach> 
			</div>
		
	<div class="customNavigation">
	  <a class="btn-floating btn-large waves-effect waves-light yellow prev"><i class="mdi-av-fast-rewind"></i></a>
	  <a class="btn-floating btn-large waves-effect waves-light yellow play"><i class="mdi-av-play-arrow"></i></a>
	  <a class="btn-floating btn-large waves-effect waves-light yellow stop"><i class="mdi-av-pause"></i></a>
	  <a class="btn-floating btn-large waves-effect waves-light yellow next"><i class="mdi-av-fast-forward"></i></a>
	</div>

    </div>
  </div>
    
    
  <div class="container">
    <div class="section">

      <!--   Icon Section   -->
      <div class="row">
        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center yellow-text"><i class="mdi-av-movie"></i></h2>
            <h5 class="center">Speeds up development</h5>

            <p class="light">We did most of the heavy lifting for you to provide a default stylings that incorporate our custom components. Additionally, we refined animations and transitions to provide a smoother experience for developers.</p>
          </div>
        </div>

        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center yellow-text"><i class="mdi-social-group"></i></h2>
            <h5 class="center">User Experience Focused</h5>

            <p class="light">By utilizing elements and principles of Material Design, we were able to create a framework that incorporates components and animations that provide more feedback to users. Additionally, a single underlying responsive system across all platforms allow for a more unified user experience.</p>
          </div>
        </div>

        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center yellow-text"><i class="mdi-av-high-quality"></i></h2>
            <h5 class="center">Easy to work with</h5>

            <p class="light">We have provided detailed documentation as well as specific code examples to help new users get started. We are also always open to feedback and can answer any questions a user may have about Materialize.</p>
          </div>
        </div>
      </div>

    </div>
  </div>


  <div class="parallax-container valign-wrapper">
    <div class="section no-pad-bot">
      <div class="container">
        <div class="row center">
          <h5 class="header col s12 light">Una moderna interfaz basadaen Material Design</h5>
        </div>
      </div>
    </div>
    <div class="parallax"><img src="resources/tickets.jpg" alt="Unsplashed background img 2"></div>
  </div>




  <!-- <div class="parallax-container valign-wrapper">
    <div class="section no-pad-bot">

        <table class="responsive-table" >
					    <thead>
					        <tr>
					            <th>Domingo</th>
					            <th>Lunes</th>
					            <th>Martes</th>
					            <th>Miercoles</th>
					            <th>Jueves</th>
					            <th>Viernes</th>
					            <th>Sabado</th>
					        </tr>
					    </thead>
					    <tbody>
					    
					    <c:forEach items="${listaHorarios}" var="horarios">
					        <tr class="active">
					            <td>${horarios.getDomingo()}</td>
					            <td>${horarios.getLunes()}</td>
					            <td>${horarios.getMartes()}</td>
					            <td>${horarios.getMiercoles()}</td>
					            <td>${horarios.getJueves()}</td>
					            <td>${horarios.getViernes()}</td>
					            <td>${horarios.getSabado()}</td>
					            
					            
					        </tr>
					        </c:forEach>					        
					    </tbody>
					</table>

    </div>
    <div class="parallax"><img src="background2.jpg" alt="Unsplashed background img 3"></div>
  </div>
  -->

   <div class="fixed-action-btn animated fadeInUpBig" style="bottom: 150px; right: 100px;">
    <a class="btn-floating btn-large yellow darken-2">
      <i class="large mdi-image-movie-creation"></i>
    </a>
    <ul>
      <li><a href="./servletListaPeliculas.do" class="btn-floating red"><i class="large mdi-maps-local-movies"></i></a></li>
      <li><a class="btn-floating yellow darken-1"><i class="large mdi-editor-format-quote"></i></a></li>
      <li><a class="btn-floating green"><i class="large mdi-action-add-shopping-cart"></i></a></li>
      <li><a class="btn-floating blue"><i class="large mdi-social-mood"></i></a></li>
    </ul>
  </div>

  <footer class="page-footer yellow">
    <div class="footer-copyright">
      <div class="container">
      <p>Made by <a href="http://kennethandre.jimdo.com">Kenneth Andr� Mart�nez Molina</a></p>
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>
  <script src="js/owl.carousel.min.js" ></script>
  <script>
  $(document).ready(function() {
 
  var owl = $("#owl-demo");
 
  owl.owlCarousel({
      items : 5, //10 items above 1000px browser width
      itemsDesktop : [1000,5], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,3], // betweem 900px and 601px
      itemsTablet: [600,2], //2 items between 600 and 0
      itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
  });
 
  // Custom Navigation Events
  $(".next").click(function(){
    owl.trigger('owl.next');
  })
  $(".prev").click(function(){
    owl.trigger('owl.prev');
  })
  $(".play").click(function(){
    owl.trigger('owl.play',1000); //owl.play event accept autoPlay speed as second parameter
  })
  $(".stop").click(function(){
    owl.trigger('owl.stop');
  })
 
});  
  
  $(document).ready(function(){
      $('.slider').slider({full_width: true});
    });
  </script>

  </body>
</html>