package com.kenneth.cine.beans;

public class Complejos {
	private Integer ID;
	private String nombreComplejo;
	private String direccionComplejo;
	private Integer callCenter;
	private String latitud;
	public Integer getID() {
		return ID;
	}
	public void setID(Integer iD) {
		ID = iD;
	}
	public String getNombreComplejo() {
		return nombreComplejo;
	}
	public void setNombreComplejo(String nombreComplejo) {
		this.nombreComplejo = nombreComplejo;
	}
	public String getDireccionComplejo() {
		return direccionComplejo;
	}
	public void setDireccionComplejo(String direccionComplejo) {
		this.direccionComplejo = direccionComplejo;
	}
	public Integer getCallCenter() {
		return callCenter;
	}
	public void setCallCenter(Integer callCenter) {
		this.callCenter = callCenter;
	}
	public String getLatitud() {
		return latitud;
	}
	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}

	public Complejos(Integer iD, String nombreComplejo,
			String direccionComplejo, Integer callCenter, String latitud) {
		super();
		ID = iD;
		this.nombreComplejo = nombreComplejo;
		this.direccionComplejo = direccionComplejo;
		this.callCenter = callCenter;
		this.latitud = latitud;
	}
	
	public Complejos() {
		super();
		// TODO Auto-generated constructor stub
	}

	
}
