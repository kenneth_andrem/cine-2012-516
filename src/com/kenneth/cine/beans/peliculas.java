package com.kenneth.cine.beans;

public class peliculas {
private String id;
private String nombre;
private String descri;
private String triler;
private String duracion;
private String imagen;

public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public String getNombre() {
	return nombre;
}
public void setNombre(String nombre) {
	this.nombre = nombre;
}
public String getDescri() {
	return descri;
}
public void setDescri(String descri) {
	this.descri = descri;
}
public String getTriler() {
	return triler;
}
public void setTriler(String triler) {
	this.triler = triler;
}
public String getDuracion() {
	return duracion;
}
public void setDuracion(String duracion) {
	this.duracion = duracion;
}
public String getImagen() {
	return imagen;
}
public void setImagen(String imagen) {
	this.imagen = imagen;
}
public peliculas() {
	super();
	// TODO Auto-generated constructor stub
}
public peliculas(String id, String nombre, String descri, String triler,
		String duracion, String imagen) {
	super();
	this.id = id;
	this.nombre = nombre;
	this.descri = descri;
	this.triler = triler;
	this.duracion = duracion;
	this.imagen = imagen;
}



}
