package com.kenneth.cine.handler;

import java.sql.ResultSet;
import java.util.ArrayList;

import com.kenneth.cine.beans.Complejos;
import com.kenneth.cine.db.DataBase;

public class complejosHandler {

	private static final complejosHandler INSTANCE = new complejosHandler();
	public static final complejosHandler getInstance(){
		return INSTANCE;
	}
	public ArrayList<Complejos> getComplejosLista(){
		ArrayList<Complejos> lista = new ArrayList<Complejos>();
		ResultSet result = DataBase.getInstance().obtenerConsulta("SELECT * FROM COMPLEJO");
		try {
			while (result.next()) {
				lista.add(new Complejos(result.getInt("IDCOMPLEJO"), result.getString("NOMBRECOMPLEJO"),
						result.getString("DIRECCIONCOMPLEJO"), result.getInt("CALLCENTER"),
						result.getString("LATITUDCOMPLEJO")));
				
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return lista;
		
	}
}
