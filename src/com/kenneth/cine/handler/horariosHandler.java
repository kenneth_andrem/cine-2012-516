package com.kenneth.cine.handler;

import java.sql.ResultSet;
import java.util.ArrayList;

import com.kenneth.cine.beans.horarios;
import com.kenneth.cine.db.DataBase;

public class horariosHandler {
	private static final horariosHandler INSTANCE = new horariosHandler();
	
		public static final horariosHandler getInstance(){
			return INSTANCE;
		}		
		
		public ArrayList<horarios> gethorarioList(){
			ArrayList <horarios> listahorarios = new ArrayList <horarios> ();
			ResultSet resultadoHorario = DataBase.getInstance().obtenerConsulta("SELECT * FROM HORARIOS");
			
			try {
				while (resultadoHorario.next()) {
					listahorarios.add(new horarios(resultadoHorario.getInt("ID"), resultadoHorario.getString("LUNES"),resultadoHorario.getString("MARTES"),resultadoHorario.getString("MIERCOLES"),resultadoHorario.getString("JUEVES"),resultadoHorario.getString("VIERNES"),resultadoHorario.getString("SABADO"),resultadoHorario.getString("DOMINGO")));					
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			return listahorarios;
				
		}
}
