package com.kenneth.cine.handler;

import java.sql.ResultSet;
import java.util.ArrayList;

import com.kenneth.cine.beans.peliculas;
import com.kenneth.cine.db.DataBase;

public class peliculasHandler {
	private static final peliculasHandler INSTANCE = new peliculasHandler();
	public static final peliculasHandler getInstance(){
		return INSTANCE;
	}
	public ArrayList<peliculas> getPeliculasLista(){
		ArrayList<peliculas> lista = new ArrayList<peliculas>();
		
		ResultSet result = DataBase.getInstance().obtenerConsulta("SELECT * FROM PELICULA");
		
		try {
			while (result.next()) {
				lista.add(new peliculas(result.getString("IDPELICULA"), result.getString("NOMBREPELICULA"),
						result.getString("DESCRIPCIONPELICULA"), result.getString("TRAILERPELICULA"),
						result.getString("DURACION"),result.getString("IMAGEN")));
				
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return lista;
	}
	
}
